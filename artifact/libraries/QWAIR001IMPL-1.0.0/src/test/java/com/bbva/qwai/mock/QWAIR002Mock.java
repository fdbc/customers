package com.bbva.qwai.mock;

import com.bbva.qwai.lib.r002.QWAIR002;

public class QWAIR002Mock implements QWAIR002{

	@Override
	public String executeDtoToBdDocType(String documentType) {
		// TODO Auto-generated method stub
		String result = "";
		if(documentType.equals("DNI")){
			result="1";
		}
		if(documentType.equals("CIF")){
			result="2";
		}
		if(documentType.equals("PASSPORT")){
			result="3";
		}
		if(documentType.equals("RESIDENTIAL_PASS")){
			result="4";
		}
		if(documentType.equals("NIF")){
			result="5";
		}
		if(documentType.equals("RUC")){
			result="6";
		}
		if(documentType.equals("CURP")){
			result="7";
		}
		if(documentType.equals("RUT")){
			result="8";
		}
		if(documentType.equals("INE")){
			result="9";
		}
		if(documentType.equals("DL")){
			result="10";
		}
		return result;
		
	}

	@Override
	public String executeBdToDtoDocType(String codTipident) {
		// TODO Auto-generated method stub
		String result = "";
		if(codTipident.equals("1")){
			result="DNI";
		}
		if(codTipident.equals("2")){
			result="CIF";
		}
		if(codTipident.equals("3")){
			result="PASSPORT";
		}
		if(codTipident.equals("4")){
			result="RESIDENTIAL_PASS";
		}
		if(codTipident.equals("5")){
			result="NIF";
		}
		if(codTipident.equals("6")){
			result="RUC";
		}
		if(codTipident.equals("7")){
			result="CURP";
		}
		if(codTipident.equals("8")){
			result="RUT";
		}
		if(codTipident.equals("9")){
			result="INE";
		}
		if(codTipident.equals("10")){
			result="DL";
		}
		return result;
	}

	@Override
	public String executeDtoToBdGender(String gender) {
		// TODO Auto-generated method stub
		String result = "";
		if(gender.equals("MALE")){
			result="V";
		}
		if(gender.equals("FEMALE")){
			result="H";
		}
		return result;
	}

	@Override
	public String executeBdToDtoGender(String xtiSexo) {
		// TODO Auto-generated method stub
		String result = "";
		if(xtiSexo.equals("V")){
			result="MALE";
		}
		if(xtiSexo.equals("H")){
			result="FEMALE";
		}
		return result;
	}

	@Override
	public String executeDtoToBdPersTitle(String personalTittle) {
		// TODO Auto-generated method stub
		String result = "";
		if(personalTittle.equals("DR")){
			result="42";
		}
		if(personalTittle.equals("MR")){
			result="15";
		}
		if(personalTittle.equals("MRS")){
			result="16";
		}
		if(personalTittle.equals("MISS")){
			result="47";
		}
		if(personalTittle.equals("MS")){
			result="48";
		}
		if(personalTittle.equals("REV")){
			result="12";
		}
		if(personalTittle.equals("SIR_MADAM")){
			result="27";
		}
		return result;
	}

	@Override
	public String executeBdToDtoPersTitle(String codTratanor) {
		// TODO Auto-generated method stub
		String result = "";
		if(codTratanor.equals("42")){
			result="DR";
		}
		if(codTratanor.equals("15")){
			result="MR";
		}
		if(codTratanor.equals("16")){
			result="MRS";
		}
		if(codTratanor.equals("47")){
			result="MISS";
		}
		if(codTratanor.equals("48")){
			result="MS";
		}
		if(codTratanor.equals("12")){
			result="REV";
		}
		if(codTratanor.equals("27")){
			result="SIR_MADAM";
		}
		return result;
	}

	@Override
	public String executeDtoToBdMaritalSt(String maritalStatus) {
		// TODO Auto-generated method stub
		String result = "";
		if(maritalStatus.equals("MARRIED")){
			result = "C";
		}
		if(maritalStatus.equals("SINGLE")){
			result = "S";
		}
		if(maritalStatus.equals("WIDOWED")){
			result = "V";
		}
		if(maritalStatus.equals("CIVIL_UNION_AGREEMENT_WITH_SEPARATED_PROPERTY")){
			result = "U";
		}
		if(maritalStatus.equals("CIVIL_UNION_AGREEMENT_WITHOUT_SEPARATED_PROPERTY")){
			result = "W";
		}
		if(maritalStatus.equals("COHABITANT")){
			result = "H";
		}
		return result;
	}

	@Override
	public String executeBdToDtoMaritalSt(String codCecivi) {
		// TODO Auto-generated method stub
		String result = "";
		if(codCecivi.equals("C")){
			result = "MARRIED";
		}
		if(codCecivi.equals("S")){
			result = "SINGLE";
		}
		if(codCecivi.equals("V")){
			result = "WIDOWED";
		}
		if(codCecivi.equals("U")){
			result = "CIVIL_UNION_AGREEMENT_WITH_SEPARATED_PROPERTY";
		}
		if(codCecivi.equals("W")){
			result = "CIVIL_UNION_AGREEMENT_WITHOUT_SEPARATED_PROPERTY";
		}
		if(codCecivi.equals("H")){
			result = "COHABITANT";
		}
		return result;
	}
	
}