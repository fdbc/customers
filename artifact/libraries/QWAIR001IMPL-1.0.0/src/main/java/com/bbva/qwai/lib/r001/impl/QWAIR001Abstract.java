package com.bbva.qwai.lib.r001.impl;

import com.bbva.qwai.lib.r002.QWAIR002;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.elara.configuration.manager.application.ApplicationConfigurationService;
import com.bbva.elara.library.AbstractLibrary;
import com.bbva.elara.utility.jdbc.JdbcUtils;
import com.bbva.qwai.dto.customers.CustomerDTO;
import com.bbva.qwai.lib.r001.QWAIR001;

public abstract class QWAIR001Abstract extends AbstractLibrary implements QWAIR001 {
	protected QWAIR002 qwaiR002;

	private static final Logger LOGGER = LoggerFactory.getLogger(QWAIR001.class);
	
	protected ApplicationConfigurationService applicationConfigurationService;
	
	protected JdbcUtils jdbcUtils;
	
	/**
	 * @param applicationConfigurationService the applicationConfigurationService to set
	 */
	public void setApplicationConfigurationService(
			ApplicationConfigurationService applicationConfigurationService) {
		this.applicationConfigurationService = applicationConfigurationService;
	}
	
	
	/**
	 * @param jdbcUtils the jdbcUtils to set
	 */
	public void setJdbcUtils(JdbcUtils jdbcUtils) {
		this.jdbcUtils = jdbcUtils;
	}
	public void setQwaiR002(QWAIR002 qwaiR002)
	{
		this.qwaiR002 = qwaiR002;
	}



}
