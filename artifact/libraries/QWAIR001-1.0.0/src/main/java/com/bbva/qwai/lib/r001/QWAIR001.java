package com.bbva.qwai.lib.r001;

import java.util.List;

import com.bbva.qwai.dto.customers.CustomerDTO;

public interface QWAIR001 {
	//-- Prueba curso --//

	List<CustomerDTO> executeGet(String docuemetType, int paginationKey, int pagSize);
	boolean executePost(CustomerDTO dto, String user, String entity);
	CustomerDTO executeGet(String customerID);

}