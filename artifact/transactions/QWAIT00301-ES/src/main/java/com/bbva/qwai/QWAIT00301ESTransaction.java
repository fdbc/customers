package com.bbva.qwai;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.elara.domain.transaction.Severity;
import com.bbva.elara.domain.transaction.response.HttpResponseCode;
import com.bbva.qwai.dto.customers.CustomerDTO;
import com.bbva.qwai.lib.r001.QWAIR001;

/**
 * cutomer-id
 * Implementacion de logica de negocio.
 * @author alumno
 *
 */
public class QWAIT00301ESTransaction extends AbstractQWAIT00301ESTransaction {
	private static final Logger LOGGER = LoggerFactory.getLogger(QWAIT00301ESTransaction.class);
	@Override
	public void execute() {
		QWAIR001 qwaiR001 = (QWAIR001)getServiceLibrary(QWAIR001.class);
		
		LOGGER.debug("nueva transaccion de prueba");
		if(QWAIT00301ESAction.GET2.name().equals(getRestfulMethod())){
			List<String> customerId = getQueryParameter("customerId");
			CustomerDTO retorno= qwaiR001.executeGet(customerId.get(0));
			this.setEntity(retorno);
			setContentLocation(getURIPath());
			setHttpResponseCode(HttpResponseCode.HTTP_CODE_200, Severity.OK);
		}
	}

}
