package com.bbva.qwai;

import java.util.List;
import com.bbva.qwai.dto.customers.CustomerDTO;

import com.bbva.elara.transaction.AbstractTransaction;

public abstract class AbstractQWAIT00301ESTransaction extends AbstractTransaction {

	public AbstractQWAIT00301ESTransaction(){
	}
	
	

	/**
	 * Return value for input parameter EntityIn
	 */
	protected CustomerDTO getEntity(){
		return (CustomerDTO)getParameter("EntityIn");
	}
	
	
	

	/**
	 * Set value for output parameter EntityList
	 */
	protected void setEntitylist(final List<CustomerDTO> field){
		this.addParameter("EntityList", field);
	}			
	
	/**
	 * Set value for output parameter EntityOut
	 */
	protected void setEntity(final CustomerDTO field){
		this.addParameter("EntityOut", field);
	}			
	
}
