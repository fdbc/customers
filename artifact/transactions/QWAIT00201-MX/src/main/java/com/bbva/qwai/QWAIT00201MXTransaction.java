package com.bbva.qwai;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.elara.domain.transaction.RequestHeaderParamsName;
import com.bbva.elara.domain.transaction.Severity;
import com.bbva.elara.domain.transaction.response.HttpResponseCode;
import com.bbva.qwai.dto.customers.CustomerDTO;
import com.bbva.qwai.lib.r001.QWAIR001;

/**
 * Transaccion para la implemnetacion del metodo Post
 * Implementacion de logica de negocio.
 * @author administradorcito
 *
 */
public class QWAIT00201MXTransaction extends AbstractQWAIT00201MXTransaction {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(QWAIT00201MXTransaction.class);
	private static final String INVALID_VALUE = "QWAI00841001";

	@Override
	public void execute() {
		QWAIR001 qwaiR001 = getServiceLibrary(QWAIR001.class);
		if (QWAIT00201MXAction.POST.name().equals(getRestfulMethod())) {
			LOGGER.debug("Ejecutando metodo POST del API Customers");
			CustomerDTO customerDTO = this.getEntity();
			if(validarCampos(customerDTO)){
				String user= this.getRequestHeader().getHeaderParameter(RequestHeaderParamsName.USERCODE).toString();
				String entity= this.getRequestHeader().getHeaderParameter(RequestHeaderParamsName.ENTITYCODE).toString();
				LOGGER.info("userCode = {} , entityCode = {}",user,entity);
				LOGGER.info("IdentityDocumentType = {}",customerDTO.getIdentityDocumentType());
				boolean result = qwaiR001.executePost(customerDTO,user,entity);
				LOGGER.debug("Tamaño del advice en transaccion = ["+this.getAdviceList().size()+"]");
				if (!this.getAdviceList().isEmpty()&&!result) {
					//QWAI00841002
					setHttpResponseCode(HttpResponseCode.HTTP_CODE_404, Severity.EWR);
				} else {
					this.setEntity(customerDTO);
					setContentLocation(getURIPath());
					setHttpResponseCode(HttpResponseCode.HTTP_CODE_201, Severity.OK);
				}
			} 
		} else {
			LOGGER.info("Operación HTTP no valida para esta TRX {} ERROR 404",getRestfulMethod());
			setHttpResponseCode(HttpResponseCode.HTTP_CODE_404, Severity.ENR);
		}
	}
	
	private boolean validarCampos(CustomerDTO customerDTO){
		boolean resultado = false;
		if(customerDTO==null){
			this.addAdvice(INVALID_VALUE, "customerDTO");
			setHttpResponseCode(HttpResponseCode.HTTP_CODE_404, Severity.ENR);
		}else if (customerDTO.getCustomerId() == null || "".equals(customerDTO.getCustomerId().trim())) {
			this.addAdvice(INVALID_VALUE, "customerId");
			setHttpResponseCode(HttpResponseCode.HTTP_CODE_404, Severity.ENR);
		} else if (customerDTO.getFirstName() == null || "".equals(customerDTO.getFirstName().trim())) {
			this.addAdvice(INVALID_VALUE, "firstName");
			setHttpResponseCode(HttpResponseCode.HTTP_CODE_404, Severity.ENR);
		} else if (customerDTO.getLastName() == null || "".equals(customerDTO.getLastName().trim())) {
			this.addAdvice(INVALID_VALUE, "lastName");
			setHttpResponseCode(HttpResponseCode.HTTP_CODE_404, Severity.ENR);
		} else if (customerDTO.getIdentityDocumentType() == null
				|| "".equals(customerDTO.getIdentityDocumentType().trim())) {
			this.addAdvice(INVALID_VALUE, "identityDocType");
			setHttpResponseCode(HttpResponseCode.HTTP_CODE_404, Severity.ENR);
		} else if (customerDTO.getIdentityDocumentNumber() == null
				|| "".equals(customerDTO.getIdentityDocumentNumber().trim())) {
			this.addAdvice(INVALID_VALUE, "identityDocNum");
			setHttpResponseCode(HttpResponseCode.HTTP_CODE_404, Severity.ENR);
		} else if (customerDTO.getBirthDate() == null) {
			this.addAdvice(INVALID_VALUE, "birthDate");
			setHttpResponseCode(HttpResponseCode.HTTP_CODE_404, Severity.ENR);
		} else if (customerDTO.getNationality() == null || "".equals(customerDTO.getNationality().trim())) {
			this.addAdvice(INVALID_VALUE, "nationality");
			setHttpResponseCode(HttpResponseCode.HTTP_CODE_404, Severity.ENR);
		} else if (customerDTO.getGenderId() == null || "".equals(customerDTO.getGenderId().trim())) {
			this.addAdvice(INVALID_VALUE, "genderId");
			setHttpResponseCode(HttpResponseCode.HTTP_CODE_404, Severity.ENR);
		}else if (customerDTO.getPersonalTitle() == null || "".equals(customerDTO.getPersonalTitle().trim())) {
			this.addAdvice(INVALID_VALUE, "personalTitle");
			setHttpResponseCode(HttpResponseCode.HTTP_CODE_404, Severity.ENR);
		}else if (customerDTO.getMaritalStatus() == null || "".equals(customerDTO.getMaritalStatus().trim())) {
			this.addAdvice(INVALID_VALUE, "maritalStatus");
			setHttpResponseCode(HttpResponseCode.HTTP_CODE_404, Severity.ENR);
		}
		else{
			resultado=true;
		}
		return resultado;
	}
}
