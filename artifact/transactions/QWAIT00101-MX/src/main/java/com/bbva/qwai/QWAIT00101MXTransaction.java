package com.bbva.qwai;
import com.bbva.qwai.lib.r001.QWAIR001;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.elara.domain.transaction.Severity;
import com.bbva.elara.domain.transaction.response.HttpResponseCode;
import com.bbva.qwai.dto.customers.CustomerDTO;

/**
 * Transaccion que implementa el metodo GET del API Customers
 * Implementacion de logica de negocio.
 * @author Administradorcito
 *
 */
public class QWAIT00101MXTransaction extends AbstractQWAIT00101MXTransaction {

	private static final Logger LOGGER = LoggerFactory.getLogger(QWAIT00101MXTransaction.class);
	
	@Override
	public void execute() {
		QWAIR001 qwaiR001 = (QWAIR001)getServiceLibrary(QWAIR001.class);
		// 
		if(QWAIT00101MXAction.GET.name().equals(getRestfulMethod())){
			LOGGER.info("Estamos entrando al metodo GET del API Customers");
			LOGGER.info("Hola Somos el group 4 the best");
			
			
			List<String> paginationKeyList = getQueryParameter("paginationKey");
			String paginationKey = null;
			LOGGER.info("Obteniendo queryPram paginationKey");
			if(paginationKeyList!=null && !paginationKeyList.isEmpty()){
				LOGGER.info("Se encontró valor de queryParam paginationKey");
				paginationKey = paginationKeyList.get(0);
			}
			else{
				LOGGER.info("Se asigna valor default a queryParam paginationKey");
				paginationKey="0";
			}
			
			List<String> pagSizeList = getQueryParameter("pageSize");
			String pagSize = null;
			if (pagSizeList != null && !pagSizeList.isEmpty()) {
				pagSize = pagSizeList.get(0);
			}
			else{
				pagSize="10";
			}

			
			List<String> identityDocumentType = getQueryParameter("identityDocumentType");
			String documentType = null;
			if (identityDocumentType != null && !identityDocumentType.isEmpty()) {
				documentType = identityDocumentType.get(0);
			}
			
			List<CustomerDTO> retorno = qwaiR001.executeGet(documentType,Integer.parseInt(paginationKey), Integer.parseInt(pagSize));
			
			if (this.getAdvice() != null) {
				if(this.getAdvice().getCode().equals("QWAI00841000")){
					LOGGER.warn("No hubo registros");
					setHttpResponseCode(HttpResponseCode.HTTP_CODE_204, Severity.WARN);
				}
				else{
					//QWAI00841002
					LOGGER.error("Hubo un error al encontrar a los clientes");
					setHttpResponseCode(HttpResponseCode.HTTP_CODE_404, Severity.ENR);
				}
			} else {
				LOGGER.debug("Busqueda exitosa");
				setEntitylist(retorno);
				setContentLocation(getURIPath());
				setHttpResponseCode(HttpResponseCode.HTTP_CODE_200, Severity.OK);
			}
			
			
			
			
//			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//			
//			List<CustomerDTO> customerList = new ArrayList<CustomerDTO>();
//			CustomerDTO customer = new CustomerDTO();
//			customer.setCustomerId("AB12345678");
//			customer.setFirstName("Gloria del Carmen");
//			customer.setLastName("Garfias Ortiz");
//			customer.setMaritalStatus("COHABIT");
//			customer.setPersonalTitle("MRS");
//			customer.setGenderId("F");
//			customer.setNationality("MX");
//			customer.setIdentityDocumentType("INE");
//			customer.setIdentityDocumentNumber("GAOG123456");
//			try {
//				customer.setBirthDate(format.parse("1989-02-28"));
//			} catch (ParseException e) {
//				LOGGER.error("Formato de fecha incorrecto");
//			}
//			customerList.add(customer);
//			this.setEntitylist(customerList);
		}
	}
}
